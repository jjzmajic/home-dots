# typeset vars
typeset -T GEMPATH gempath
typeset -T NODE_PATH node_path
typeset -U path manpath node_path gempath

# lang
export NPM_PACKAGES="${HOME}/node"
node_path+=("${NPM_PACKAGES}/lib/node_modules")
manpath+=(${NPM_PACKAGES}/share/man)
path+=(${NPM_PACKAGES}/bin)

export GOPATH="${HOME}/go"
path+=(${GOPATH}/bin)

export GEMPATH=$(gem environment gempath)
path+=(${gempath[1]}/bin)

# misc
path+=(~/.nimble/bin)
path+=(~/.roswell/bin)
path+=(~/.cargo/bin)
fpath+=($(rustc --print sysroot)/share/zsh/site-functions)

# personal
path+=(/usr/local/bin ~/bin ~/.local/bin)
path+=(~/.emacs.d/bin)

# history config
export HISTFILE=~/.zhistory
export HISTSIZE=10000
export SAVEHIST=10000

# default programs
export EDITOR="emacsclient -ca ''"
export SUDO_EDITOR="emacsclient -ca ''"
export VISUAL="emacsclient -ca ''"

# plugins
POWERLEVEL9K_MODE='nerdfont-fontconfig'
POWERLEVEL9K_IGNORE_TERM_COLORS=true
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR=''
POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR=''
POWERLEVEL9K_PROMPT_ON_NEWLINE=false
POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND='white'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='blue'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='black'
POWERLEVEL9K_EXECUTION_TIME_ICON='\ufc8a'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=1
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs newline
				   status command_execution_time
				   dir_writable virtualenv anaconda
				   background_jobs history)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()
ZSH_PECO_HISTORY_OPTS="--layout=top-down"

# misc
export QT_QPA_PLATFORMTHEME="gtk2"
export STARDICT_DATA_DIR="~/store/my_sync/stardict"
export DATE=`date '+%Y-%m-%d_%H:%M:%S'`
export PROJECT_DIR=~/projects
HEROKU_AC_ZSH_SETUP_PATH=/home/uros/.cache/heroku/autocomplete/zsh_setup
WORDCHARS=':'
