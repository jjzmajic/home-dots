import os, subprocess

from libqtile import bar, hook, layout, widget
from libqtile.command import lazy
from libqtile.config import Click, Drag, Group, Key, Screen

from qwal import accent_colors
from qwal import background as background_color
from qwal import default_theme
from qwal import foreground as foreground_color
from qwal import set_wallpaper, uniqueish_color

try:
    from typing import List  # noqa: F401
except ImportError:
    pass

# keys---annotations added for clarification and mnemonics
mod = "mod4"  # universal leader
alt = "mod1"  # qtile
ctrl = "control"  # layouts
shift = "shift"  # related to command

keys = [
    # callback sequence: bsp -> monad_tall -> tbd
    # navigation
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "l", lazy.layout.right()),
    # movement
    Key([mod, shift], "h", lazy.layout.shuffle_left(), lazy.layout.swap_left()),
    Key([mod, shift], "j", lazy.layout.shuffle_down()),
    Key([mod, shift], "k", lazy.layout.shuffle_up()),
    Key([mod, shift], "l", lazy.layout.shuffle_right(), lazy.layout.swap_right()),
    # shuffling
    Key([mod, ctrl, shift], "h", lazy.layout.flip_left()),  # monad
    Key([mod, ctrl, shift], "j", lazy.layout.flip_down()),  # monad
    Key([mod, ctrl, shift], "k", lazy.layout.flip_up()),  # monad
    Key([mod, ctrl, shift], "l", lazy.layout.flip_right()),  # monad
    # resizing
    Key([mod, ctrl], "h", lazy.layout.grow_left(), lazy.layout.shrink_main()),
    Key([mod, ctrl], "j", lazy.layout.grow_down(), lazy.layout.shrink()),
    Key([mod, ctrl], "k", lazy.layout.grow_up(), lazy.layout.grow()),
    Key([mod, ctrl], "l", lazy.layout.grow_right(), lazy.layout.grow_main()),
    Key([mod, ctrl], "n", lazy.layout.normalize()),
    Key([mod, ctrl], "s", lazy.layout.toggle_split()),  # monad
    Key([mod, ctrl], "o", lazy.layout.maximize()),  # monad
    Key([mod, ctrl], "Return", lazy.layout.flip()),  # monad
    # programs
    Key([mod], "q", lazy.spawn("termite -e zsh")),  # Quick (easier than t)
    Key([mod, shift], "q", lazy.spawn("termite -e fish")),  # fish
    Key([mod], "w", lazy.spawn("qutebrowser")),  # Web
    Key([mod, shift], "w", lazy.spawn("google-chrome-stable")),  # compatibility
    Key(
        [mod, ctrl, shift], "w", lazy.spawn("google-chrome-stable --incognito")
    ),  # wink-wink
    Key([mod], "e", lazy.spawn("emacsclient -c")),  # Emacs
    Key([mod, shift], "e",
        lazy.spawn(os.path.expanduser("~/.emacs_anywhere/bin/run"))),
    Key([mod, alt, shift], "e",
        lazy.spawn("emacs --daemon")),  # if service breaks
    Key([mod], "r", lazy.spawn("termite -e ranger")),  # Ranger
    Key([mod, shift], "r", lazy.spawn("pcmanfm-qt")),  # attaching files online
    Key([mod], "t", lazy.spawn("termite -e calcurse")),  # Time
    Key([mod], "c", lazy.spawn("termite -e cmus")),  # musiC
    # rofi
    Key([mod], "m", lazy.spawn("rofi -show drun -show-icons")),  # Menu
    Key([mod], "n", lazy.spawn("rofi -show window -show-icons")),  # wiNdow
    Key([mod], "b", lazy.spawn("rofi -show run")),  # proximate key, sue me
    # qtile
    Key([mod, alt], "h", lazy.prev_layout()),
    Key([mod, alt], "l", lazy.next_layout()),
    Key([mod, alt], "t", lazy.screen.toggle_group()),  # Toggle
    Key([mod, alt], "o", lazy.window.toggle_fullscreen()),  # Open wide
    Key([mod, alt], "f", lazy.window.toggle_floating()),  # Floating
    # theming qtile
    Key([mod, alt], "r", lazy.restart()),  # Restart
    Key(
        [mod, alt], "n", lazy.spawn("wal --theme random"), lazy.restart()
    ),  # Next color-scheme
    Key(
        [mod, alt],
        "d",
        lazy.spawn("wal --theme {theme}".format(theme=default_theme)),
        lazy.restart(),
    ),  # Default color-scheme
    # theming everything
    Key(
        [mod, alt, shift],
        "r",
        lazy.restart(),
        lazy.spawn("emacsclient -e '(dotspacemacs/sync-configuration-layers)'"),
        lazy.spawn("qutebrowser :config-source"),
    ),  # Restart
    Key(
        [mod, alt, shift],
        "n",
        lazy.spawn("wal --theme random"),
        lazy.spawn("emacsclient -e '(dotspacemacs/sync-configuration-layers)'"),
        lazy.spawn("qutebrowser :config-source"),
        lazy.restart(),
    ),  # Next color-scheme
    Key(
        [mod, alt, shift],
        "d",
        lazy.spawn("wal --theme {theme}".format(theme=default_theme)),
        lazy.spawn("emacsclient -e '(dotspacemacs/sync-configuration-layers)'"),
        lazy.spawn("qutebrowser :config-source"),
        lazy.restart(),
    ),  # Default color-scheme
    Key([mod, alt], "q", lazy.shutdown()),  # Quit
    Key([mod], "z", lazy.window.kill()),  # neXt to X and easier to reach
    Key([mod, alt], "b", lazy.hide_show_bar()),  # Bar
    # hwkeys
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn(
        "amixer -c 0 sset Master 1- unmute")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn(
        "amixer -c 0 sset Master 1+ unmute")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10")),
]

group_spawns = [None, None, None, None, None, None, None, None]
group_matches = [None, None, None, None, None, None, None, None]
group_labels = [None, None, None, None, None, None, None, None]
group_names = list("asdfuiop")

groups = [
    Group(name=group_name, label=group_label,
          spawn=group_spawn, matches=group_match)
    for group_name, group_label, group_spawn, group_match in zip(
        group_names, group_labels, group_spawns, group_matches
    )
]

for i in groups:
    keys.extend(
        [
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        ]
    )

linewidth = 4
sep_height = 100
margin = 0
fontsize = 14
font = "Source Code Pro Bold"
icon_font = "Symbols Nerd Font"
wallpaper_color = accent_colors.pop()
color = uniqueish_color(accent_colors)
layout_color = next(color)
groupbox_color = next(color)
systray_color = next(color)
checkupdates_color = next(color)
volume_color = next(color)
backligth_color = next(color)
battery_color = next(color)
calendar_color = next(color)
clock_color = next(color)
keyboard_color = next(color)
notify_color = next(color)

set_wallpaper(wallpaper_color)

layouts = [
    layout.xmonad.MonadTall(
        border_focus=foreground_color,
        border_normal=wallpaper_color,
        border_width=linewidth,
        margin=margin,
        align=0,
        new_at_current=False,
        min_ratio=0.18,
        max_ratio=0.82,
        ratio=0.58,
    ),
    layout.bsp.Bsp(
        border_focus=foreground_color,
        border_normal=wallpaper_color,
        border_width=linewidth,
        margin=margin,
        grow_amount=linewidth,
    ),
]

widget_defaults = dict(
    font=font,
    fontsize=fontsize,
    foreground=foreground_color,
    fontshadow=background_color,
    padding=linewidth,
    margin=linewidth,
    background=background_color,
)

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(background=layout_color),
                widget.GroupBox(
                    rounded=False,
                    foregroundwidth=linewidth,
                    highlight_method="line",
                    highlight_color=[
                        groupbox_color,
                        foreground_color
                    ],
                    this_current_screen_border=foreground_color,
                    center_aligned=True,
                    background=groupbox_color,
                ),
                # ---
                widget.Sep(
                    size_percent=sep_height,
                    padding=margin,
                    linewidth=linewidth,
                    foreground=wallpaper_color
                ),
                widget.WindowName(),
                widget.Sep(
                    size_percent=sep_height,
                    padding=margin,
                    linewidth=linewidth,
                    foreground=wallpaper_color
                ),
                # ---
                widget.Systray(background=systray_color),
                widget.TextBox(
                    font=icon_font,  # extra icons
                    background=checkupdates_color,
                    text="\uf0aa",
                ),
                widget.CheckUpdates(
                    distro="Arch_checkupdates",
                    update_interval=30,
                    colour_have_updates=foreground_color,
                    colour_no_updates=foreground_color,
                    display_format="{updates}",
                    background=checkupdates_color,
                ),
                widget.TextBox(
                    font=icon_font,
                    background=volume_color,
                    text="\ufac4"
                ),
                widget.Volume(background=volume_color),
                widget.TextBox(
                    font=icon_font,
                    background=backligth_color,
                    text="\uf5df"
                ),
                widget.Backlight(
                    backlight_name="intel_backlight",
                    format="{percent:2.0%}",
                    background=backligth_color,
                ),
                widget.TextBox(
                    font=icon_font,
                    background=battery_color,
                    text="\uf583"
                ),
                widget.Battery(
                    background=battery_color,
                    charge_char="+",
                    discharge_char="-",
                    format="{char} {percent:2.0%} {hour:d}:{min:02d}",
                ),
                widget.TextBox(
                    font=icon_font,
                    background=calendar_color,
                    text="\uf2d3"
                ),
                widget.Clock(
                    format="%Y-%m-%d %a",
                    background=calendar_color
                ),
                widget.TextBox(
                    font=icon_font,
                    background=clock_color,
                    text="\ufc8a"
                ),
                widget.Clock(
                    format="%I:%M %P",
                    background=clock_color
                ),
                widget.TextBox(
                    font=icon_font,
                    background=keyboard_color,
                    text="\uf80b"
                ),
                widget.KeyboardLayout(
                    configured_keyboards=["us", "rs", "ru"],
                    background=keyboard_color
                ),
                widget.Notify(
                    default_timeout=4,
                    background=notify_color
                ),
            ],
            24,
        )
    )
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size()
    ),
    Click([mod],
          "Button2",
          lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(
    float_rules=[],
    border_focus=foreground_color,  # more theming
    border_normal=background_color,
    border_width=4,
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# float_rules is broken
@hook.subscribe.client_new
def tile_all(window):
    window.floating = False

# autostart programs
@hook.subscribe.startup_once
def autostart():
    sh = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([sh])

# make intellij work
wmname = "LG3D"
