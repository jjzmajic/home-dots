# get colors from pywal cache and work some magic
import json
import os
import random
from wand.image import Image

import pywal
from colour import Color

default_theme = 'sexy-material'


def set_wal_theme(theme=default_theme):
    if theme == 'random':
        theme_file = pywal.theme.get_random_theme()
    else:
        theme_name = theme + '.json'
        all_themes = pywal.theme.list_themes()
        for theme in all_themes:
            if theme.name == theme_name:
                theme_file = theme.path

    if not theme_file:
        raise KeyError('Invalid theme name.')

    theme_wal = pywal.theme.file(theme_file)
    pywal.sequences.send(theme_wal)
    pywal.export.every(theme_wal)
    pywal.reload.env()


wal_cache = pywal.export.CACHE_DIR
wal_cache_json = os.path.join(wal_cache, 'colors.json')


def get_current_theme(json_theme=wal_cache_json):
    if os.path.isfile(wal_cache_json):
        with open(json_theme) as theme_file:
            theme = json.load(theme_file)
            return theme
    else:
        # if cache is empty search recursively
        set_wal_theme()
        get_current_theme()


theme = get_current_theme()
background = theme['special']['background']
foreground = theme['special']['foreground']
colors = theme['colors']


# only indices 1-6 are unique and distinct from the background
def filter_accent_colors(colors, colors_to_keep=(1, 2, 3, 4, 5, 6)):
    accent_color_keys = ['color' + str(num) for num in colors_to_keep]
    accent_colors = [colors[color_key] for color_key in accent_color_keys]
    random.shuffle(accent_colors)
    return accent_colors


# get extra colors by messing with saturation
def get_extra_accent_colors(accent_colors):
    colors = [Color(accent_color) for accent_color in accent_colors]
    for color in colors:
        if color.saturation < 0.5:
            color.saturation = 2 * color.saturation
        else:
            color.saturation = 0.5 * color.saturation
    hex_colors = [color.hex_l for color in colors]
    return hex_colors


# get extra colors by messing with interpolation
def get_between_accent_colors(accent_colors):
    colors = [Color(accent_color) for accent_color in accent_colors]
    extra_colors = []
    for c1, c2 in zip(colors[::2], colors[1::2]):
        extra_colors.append(list(c1.range_to(c2, 3))[1])
    colors = colors[1:] + [colors[0]]
    for c1, c2 in zip(colors[::2], colors[1::2]):
        extra_colors.append(list(c1.range_to(c2, 3))[1])
    hex_colors = [color.hex_l for color in extra_colors]
    return hex_colors


# be basic and don't use extra colors
accent_colors = filter_accent_colors(colors)


# make sure colors look more random than random things are
def uniqueish_color(colors):
    seen_colors = set()
    previous_color = ''
    while True:
        color = random.choice(colors)
        if len(seen_colors) == len(colors):
            seen_colors = set()
        if color not in seen_colors and color != previous_color:
            seen_colors.add(color)
            previous_color = color
            yield color


def set_wallpaper(background):
    wallpaper_file = os.path.expanduser('~/wallpapers/solid.png')
    os.makedirs(os.path.dirname(wallpaper_file), exist_ok=True)
    with Image(width=1920, height=1080, background=background) as img:
        img.save(filename=wallpaper_file)
    feh_command = 'feh --bg-fill {file}'.format(file=wallpaper_file)
    os.system(feh_command)
