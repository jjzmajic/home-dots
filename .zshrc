# aliases
alias aw=arch-wiki
alias ipy=ipython
alias r='systemctl reboot'
alias w=startx # window system
alias ec='emacsclient -t'
alias make='command make'
alias -g update-mirrors='reflector --latest 200 --sort rate \
--save /etc/pacman.d/mirrorlist'

# utils
exec-p() { type "$1" > /dev/null 2>&1; }
source-maybe() { test -f "$1" && source "$1" }

# zplg
if [[ ! -d "${HOME}/.zplugin" ]]; then
    mkdir ~/.zplugin && \
        git clone --depth 1 \
            https://github.com/zdharma/zplugin.git ~/.zplugin/bin
fi
source ~/.zplugin/bin/zplugin.zsh

# pkg wrappers
alias yas="yay -Slq |
fzf -m --preview 'yay -Si {1}' --preview-window=right:70%:wrap |
xargs -ro yay -S"
alias yac="yay -Slq --aur |
fzf -m --preview 'aur-talk {1}' --preview-window=right:70%:wrap |
xargs -ro yay -S"
if exec-p xbps-install; then
    zplg ice wait"0" lucid pick"vpm" as"program"
    zplg light netzverweigerer/vpm
fi

# prezto plugins
zplg ice wait"0" lucid blockf
zplg snippet PZT::modules/completion/init.zsh
bindkey '^[[Z' reverse-menu-complete
zplg ice wait"0" lucid
zplg snippet PZT::modules/ssh/init.zsh
zplg ice wait"0" lucid
zplg snippet PZT::modules/terminal/init.zsh
zstyle ':prezto:module:terminal' auto-title 'yes' # config
zplg ice wait"0" lucid
zplg snippet PZT::modules/directory/init.zsh
zstyle ':prezto:*:*' color 'yes' # config

# oh-my-zsh plugins
zplg ice wait"0" lucid
zplg snippet OMZ::plugins/aws/aws.plugin.zsh
zplg ice wait"0" lucid
zplg snippet OMZ::plugins/thefuck/thefuck.plugin.zsh
zplg ice wait"0" lucid
zplg snippet OMZ::plugins/colored-man-pages/colored-man-pages.plugin.zsh

# misc plugins
zplg ice lucid depth"1" pick"powerlevel10k.zsh-theme"
zplg light romkatv/powerlevel10k
zplg ice wait"0" lucid from"gh-r" as"program"
zplg light junegunn/fzf-bin
zplg ice wait"0" lucid silent
zplg snippet https://github.com/junegunn/fzf/raw/master/shell/key-bindings.zsh
zplg ice wait"0" lucid silent
zplg snippet https://github.com/junegunn/fzf/raw/master/shell/completion.zsh
zplg ice wait"0" lucid
zplg light thetic/extract
zplg ice as"program" make"install PREFIX=$ZPFX"
zplg light Xfennec/progress
zplg ice wait"0" lucid as"program" pick"wd.sh" \
        mv"_wd.sh -> _wd" \
        atload"wd() { source wd.sh }" blockf
zplg light mfaerevaag/wd
zplg ice wait"0" lucid
zplg light hlissner/zsh-autopair
zplg ice wait"0" lucid blockf
zplg light zsh-users/zsh-completions
zplg ice wait"0" as"completion" lucid silent
zplg snippet https://github.com/esc/conda-zsh-completion/raw/master/_conda
zplg ice wait"0" depth"1" silent \
     mv"yadm.zsh_completion -> _yadm"
zplg light TheLocehiliosan/yadm
zplg ice wait"1" lucid atload"_zsh_autosuggest_start"
zplg light zsh-users/zsh-autosuggestions
zplg ice wait"0" depth"1" lucid
zplg light zdharma/fast-syntax-highlighting
zplg ice wait"1" lucid # after syntax highlighting
zplg light zsh-users/zsh-history-substring-search
zplg ice lucid wait"0"
# zplg fpath knu/zsh-manydots-magic && \
#     autoload -Uz manydots-magic && \
#     manydots-magic

# personal plugins
zplg ice wait"0" lucid from"gitlab"
zplg light jjzmajic/zsh-functions

# completions
zpcompinit # zplg define compdef
compdef _gnu_generic bfg neofetch nim nimble ranger sdcv \
    stow wal grub-install doom
compdef _crontab fcrontab
_rustup() { source <(rustup completions zsh) }
zpcdreplay # zplg replay completions
# zstyle 'completion:*' rehash true

# external programs
test -f ~/.cache/wal/sequences && (cat ~/.cache/wal/sequences &)
source-maybe ~/.cache/wal/colors.sh
source-maybe $HEROKU_AC_ZSH_SETUP_PATH
source-maybe ~/.opam/opam-init/init.zsh
source-maybe /opt/miniconda3/etc/profile.d/conda.sh
exec-p direnv && eval "$(direnv hook zsh)"
exec-p register-python-argcomplete && \
    autoload bashcompinit && bashcompinit && \
    eval "$(register-python-argcomplete textract)"
exec-p update-grub || alias -g update-grub='grub-mkconfig -o /boot/grub/grub.cfg'
exec-p thefuck && zstyle ':prezto:module:utility' correct 'no'

exa-ls () {
    if exec-p exa; then
        alias l='exa -1'
        alias la='exa -l --all --group-directories-first --git'
        alias lt='exa -lT --all --level=2 --group-directories-first'
        alias lp='exa -lT --git --git-ignore --level=2 --group-directories-first'
    fi
}
zplg ice wait"0" lucid silent blockf svn atload"exa-ls"
zplg snippet PZT::modules/utility
